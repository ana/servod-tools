# Take me to your Servo

This set of tools lets you use a
[Servo](https://www.chromium.org/chromium-os/servo) control board for
Chromebooks somewhat automatically.  After installing these and configuring for
your local boards, you should have access to the CPU UART as
`/dev/google-servo/$devicename/cpu-uart`, and the EC as
`/dev/google-servo/$devicename/ec-uart`, as well as having `servod` itself
listening for more complex commands on a predictable port.


# Dependencies

## hdctools

In order to actually communicate with the servo, you'll need to have
[hdctools](https://chromium.googlesource.com/chromiumos/third_party/hdctools)
installed somewhere we can see it.

However, recent versions are not stand-alone but have some dependencies on
other ChromeOS projects and realistically need to be built within a full Chrome
OS source tree.  So for this particular case, here's a branch that is known to
work on Debian Stretch:

```
git clone https://gitlab.collabora.com/gtucker/hdctools.git -b collabora-lab-b
```

You'll also need `libftdi-dev`, `tidy`, and `python-setuptools`.

Then to build and install it:

```
make
sudo make install
python setup.py build
sudo python setup.py install
```

## ec (embedded controller)

This `servod-tools` package also depends on the
[ec](https://chromium.googlesource.com/chromiumos/third_party/hdctools) package
to be built and installed.  This is the code for the embedded controller, it is
mostly needed here to access some of the Python modules it includes.

Building this requires several development packages to be installed, in
particular a `gcc-arm-none-eabi` toolchain.

Again, here's a branch that is known to work on Debian Stretch (to build and
install it outside of a Chrome OS tree):

```
git clone https://gitlab.collabora.com/gtucker/ec.git -b collabora-lab-b
```

The steps to build and install it are:

```
make
python setup.py build
sudo python setup.py install
```

# Set up and configuration

## udev

The udev rule will match the Servo and Servo Micro boards to start `servod`
using `systemd`. Install it to `/etc/udev/rules.d/`.

## systemd

As above, but this goes in `/etc/systemd/system/`.

## servod configuration

The configuration, in `/etc/google-servo.conf` takes the form:

```
localalias,serial,port,boardtype
```

### localalias

is what gets aliased into `/dev/google-servo/$devicename/*`,
e.g. `peach-pi-on-my-desk`, `veyron-jaq-in-the-box`, `big-one`,
`gru-kevin-two`.  You get the picture.  The serial number comes from
`/sys/devices/*/serial`; I recommend discovering this with:

```
udevadm info --attribute-walk --name /dev/usb/google-servoN
```

The immediate parent of that device shown, should have an `ATTR{serial}` entry
in the form of "123456-12345".

It's also possible to get this information from `dmesg` after plugging the
Servo board with the device attached to it.  For example:

```
sudo dmesg | grep 18d1 -A4
[    7.945500] usb 3-2.2.2.5: New USB device found, idVendor=18d1, idProduct=5002
[    7.945505] usb 3-2.2.2.5: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[    7.945508] usb 3-2.2.2.5: Product: servo:810-10010-02
[    7.945511] usb 3-2.2.2.5: Manufacturer: Google Inc
[    7.945513] usb 3-2.2.2.5: SerialNumber: 905537-00233
```

### port

is the port number to bind to for `dut-control` to use later.

### boardtype

is an internal value used by servo to work out how to communicate with the
target device; see `hdctools/servo/data/` for something resembling a list of
possible values.

## servod wrapper

This fairly trivial wrapper runs out of systemd and creates the device links
for `/dev/google-servo/$devicename/*`.  You'll need `python-numpy`,
`python-pexpect`, `python-pyudev`, `python-serial`, and `python-usb` to run it.

Install it as `/usr/bin/run-servod`.

# Putting it all together

Once you've done all this, servod and serial consoles should automagically turn
up whenever you plug your board in, and disappear once you haven't.  Brilliant.
